# ggez boilerplate
This is an alternative boilerplate for the Rust game-creation library called [ggez](https://github.com/ggez/ggez). The one on their repository is single-file. This is slightly more organized.

## Before you start developing
Don't forget to do these before you start working on the game.

1. Modify `src/Cargo.toml` to put your name and your game's name in it
2. Modify `src/main.rs` to replace `crate_name` with the name you put in `Cargo.toml`
3. (Optional) Rewrite this README
4. Edit `src/lib.rs` to change the title, label and author.

## What each file does
### 1. `main.rs`
Error handling and launching the app, nothing that you really have to modify.

### 2. `lib.rs`
Declares all modules and constants

### 3. `object.rs`
Declares the `Object` struct and all shape creation functions.

#### 3.1. `object/object_type.rs`
All the types of objects that exist.

### 4. `game.rs`
Declares the `Game` stuct. Modify to change game state variables.

### 5. `launch.rs`
Contains code for launching the game. Edit this to change the window configuration.

### 6. `setup.rs`
Setup functions for different parts of the game
