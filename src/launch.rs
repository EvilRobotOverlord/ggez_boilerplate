use crate::{game::Game, AUTHOR, GAME_ID, TITLE};

use ggez::{
    conf::{WindowMode, WindowSetup},
    event::run,
    ContextBuilder,
};

use crate::setup;
use std::error::Error;

pub fn launch() -> Result<(), Box<Error>> {
    let (mut ctx, mut event_loop) = ContextBuilder::new(GAME_ID, AUTHOR)
        .window_setup(WindowSetup::default().title(TITLE))
        .window_mode(WindowMode::default().dimensions(800.0, 450.0))
        .build()?;

    let mut game = Game::new();
    setup::initial(&mut game, &mut ctx)?;
    run(&mut ctx, &mut event_loop, &mut game)?;

    Ok(())
}
