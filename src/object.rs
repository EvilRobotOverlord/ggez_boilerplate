pub mod object_type;

use self::object_type::ObjectType;

use ggez::{
    graphics::{draw, Color, DrawMode, DrawParam, Drawable, Mesh, Rect},
    Context, GameResult,
};

use nalgebra::Point2;

pub struct Object {
    obj_type: ObjectType,
    pub params: DrawParam,
}

fn draw_obj<T>(ctx: &mut Context, drawable: &T, owner: &Object) -> GameResult
where
    T: Drawable,
{
    draw(ctx, drawable, owner.params)
}

impl Object {
    pub fn circle(
        ctx: &mut Context,
        style: DrawMode,
        coords: Point2<f32>,
        radius: f32,
        color: Color,
    ) -> GameResult<Object> {
        Ok(Object {
            obj_type: ObjectType::Mesh(Mesh::new_circle(ctx, style, coords, radius, 0.25, color)?),
            params: DrawParam::default().dest(coords),
        })
    }

    pub fn rectangle(
        ctx: &mut Context,
        style: DrawMode,
        bounds: Rect,
        color: Color,
    ) -> GameResult<Object> {
        Ok(Object {
            obj_type: ObjectType::Mesh(Mesh::new_rectangle(ctx, style, bounds, color)?),
            params: DrawParam::default().dest(Point2::new(bounds.x, bounds.y)),
        })
    }

    pub fn line(
        ctx: &mut Context,
        points: &[Point2<f32>],
        width: f32,
        color: Color,
    ) -> GameResult<Object> {
        Ok(Object {
            obj_type: ObjectType::Mesh(Mesh::new_line(ctx, points, width, color)?),
            params: DrawParam::default().dest(points[0]),
        })
    }

    pub fn draw(&self, ctx: &mut Context) -> GameResult {
        match &self.obj_type {
            // it doesn't really matter which enum it is, just draw it
            // it's going to be like this till `Drawable` can be `Box`-ed
            ObjectType::Text(obj) => draw_obj(ctx, obj, &self),
            ObjectType::Mesh(obj) => draw_obj(ctx, obj, &self),
        }
    }

    pub fn param(&mut self, params: DrawParam) {
        self.params = params;
    }
}
