use crate_name::launch::launch;
use std::process::exit;

fn main() {
    if let Err(e) = launch() {
        eprintln!("Error: {}", e);
        exit(1);
    }
}
