pub mod game;
pub mod launch;
pub mod object;

/// Functions for setting up different states of the game
pub mod setup;

pub const GAME_ID: &str = "abc";
pub const AUTHOR: &str = "me";
pub const TITLE: &str = "game";
