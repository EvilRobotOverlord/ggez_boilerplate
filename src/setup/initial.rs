use crate::game::Game;
use crate::object::Object;

use ggez::{
    graphics::{DrawMode, Rect, WHITE},
    Context, GameResult,
};

pub fn initial(game: &mut Game, ctx: &mut Context) -> GameResult {
    game.add(Object::rectangle(
        ctx,
        DrawMode::Fill,
        Rect::new(100., 100., 100., 100.),
        WHITE,
    )?);

    Ok(())
}
