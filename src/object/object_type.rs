use ggez::graphics;

/// `Drawable` can't be `Box`-ed, so this is used instead.
pub enum ObjectType {
    Mesh(graphics::Mesh),
    Text(graphics::Text),
}
