use ggez::{
    event::EventHandler,
    graphics::{clear, present, BLACK},
    Context, GameResult,
};

use crate::object::Object;

/// Main struct that contains all the objects and holds the current state of the game
pub struct Game {
    /// List of all objects
    pub objects: Vec<Object>,
}

impl Game {
    pub fn new() -> Game {
        Game { objects: vec![] }
    }
}

impl Game {
    pub fn add(&mut self, obj: Object) {
        self.objects.push(obj);
    }
}

impl EventHandler for Game {
    fn update(&mut self, _: &mut Context) -> GameResult {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        clear(ctx, BLACK);

        for obj in &self.objects {
            obj.draw(ctx)?;
        }

        present(ctx)?;
        Ok(())
    }
}
